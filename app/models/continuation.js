import DS from 'ember-data';

export default DS.Model.extend({
  toVideo: DS.belongsTo('video', { inverse: null }),
  component: DS.belongsTo('listcomp'),
});
