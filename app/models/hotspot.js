import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  component: DS.belongsTo('listcomp'),
  toVideo: DS.belongsTo('video', { inverse: null }),
  canvasElement: DS.attr()
});
