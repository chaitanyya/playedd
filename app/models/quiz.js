import DS from 'ember-data';

export default DS.Model.extend({
  question: DS.attr(),
  options: DS.hasMany('option'),
  top: DS.attr(),
  component: DS.belongsTo('listcomp'),
  left: DS.attr()
});
