import Ember from 'ember';

export default Ember.Controller.extend({
  // Hotspot Components
  ActiveCanvas: null,
  isPreview: false,
  initialised: false,
  QuizOptionCount: 2,

  LoadConfig: function() {
    var controller = this;
    Ember.run.scheduleOnce('afterRender', this, function(){
      NProgress.configure({ easing: 'ease', speed: 700, showSpinner: false });
      videojs("MainPlayer").on("sliderchange",function() {
        var values = videojs("MainPlayer").getValueSlider();
        controller.set('startTimeHandle', values.start.toFixed(2));
        controller.set('endTimeHandle', values.end.toFixed(2));
      });
    });
  }.on('init'),

  actions: {
    addHotspot: function() {
      NProgress.start();
      
      // Add reference of the Hotspot in the HotspotList
      var hotspotElement = new fabric.Rect({
        left: 200,
        top: 400,
        fill: 'rgba(255, 30, 81, 0.6)',
        width: 220,
        height: 100,
        scaleX: 2.784954604409857,
        scaleY: 1.3501393548748069
      });
      
      // Crate a new Ember Data model object for hotspot
      var newHotspot = this.store.createRecord('hotspot', {
        title: this.get('HotspotName'),
        canvasElement: hotspotElement,
        toVideo: this.get('HotspotVideo')
      });

      // Draw the hotspot on the ActiveCanvas
      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').add(newHotspot.get('canvasElement'));
      });

      var newListComp = this.store.createRecord('listcomp', {
        title: this.get('HotspotName'),
        comptype: 1,
        starttime: this.get('startTimeHandle'),
        endtime: this.get('endTimeHandle'),
        hotspot: newHotspot
      });

      this.store.find('video', this.get('ActiveCanvas')).then(function(onVideo) {
        onVideo.get('components').pushObject(newListComp);
      });

      $('#md-link').modal('toggle');
      
      this.set('initialised', true);
      
      NProgress.done();
    },

    SocialMedia: function () {
      NProgress.start();
      
      var newSocialButtons = this.store.createRecord('social', {
        facebook: this.get('facebookLink'),
        twitter: this.get('twitterLink')
      });

      var newListComp = this.store.createRecord('listcomp', {
        title: "Socail Media Buttons",
        comptype: 3,
        starttime: this.get('startTimeHandle'),
        endtime: this.get('endTimeHandle'),
        social: newSocialButtons
      });

      this.store.find('video', this.get('ActiveCanvas')).then(function(onVideo) {
        onVideo.get('components').pushObject(newListComp);
      });

      Ember.$("#html_"+this.get('ActiveCanvas')).append(
        '<div id="' + newListComp.get('id') + '" class="SocialMedia animated bounceIn">' +
        '<h3>Share this experience with your friends!</h3><Br>' +
        '<button class="btn btn-primary btn-lg m-r-sm" type="button" onclick="window.open(\'http://'+this.get('facebookLink')+'\')"><i class="fa fa-facebook"></i> Share it on Facebook</button>' +
        '<button class="btn btn-theme-inverse btn-lg" type="button" onclick="window.open(\'http://'+this.get('twitterLink')+'\')"><i class="fa fa-twitter"></i> Share it on Twitter</button>' +
        '</div>'
        );

      $("#"+newListComp.get('id')).draggable({ scroll: false });
      
      $('#md-socialHotspot').modal('toggle');
      
      this.set('initialised', true);
      
      NProgress.done();
    },

    addQuiz: function() {
      NProgress.start();
      
      var data = Ember.$("#QuizForm").serializeArray();

      var question = data[0]['value'];
      
      var newQuiz = this.store.createRecord('quiz', {
        question: question
      });

      var newListComp = this.store.createRecord('listcomp', {
        title: question,
        comptype: 2,
        starttime: this.get('startTimeHandle'),
        endtime: this.get('endTimeHandle'),
        quiz: newQuiz
      });

      this.store.find('video', this.get('ActiveCanvas')).then(function(onVideo) {
        onVideo.get('components').pushObject(newListComp);
      });

      var controller = this;

      Ember.$.each(data, function(key, option) {
        if (key === 0) {
          Ember.$("#html_"+controller.get('ActiveCanvas')).append(
            '<section class="quizTemplate panel animated bounceIn" id="' + newListComp.get('id') + '">' +
            '<header class="panel-heading xs">' +
            '<h2><strong>' + question + '</h2>' +
            '</header>' +
            '<div class="panel-body" id="options_' + newListComp.get('id') + '">' +
            '</div>' +
            '</section>'
          );
        } else {
          var optionValue = option.value;

          Ember.$('#options_' + newListComp.get('id')).prepend(
            '<div class="quizOption">' +
            '<button for="o1" type="button" class="btn clickActive"><i class="fa fa-check optionButton"></i></button>' +
            '<span class="quizOptionText"> ' + optionValue + '</span>' +
            '</div>'
            );

          var newQuizOption = controller.store.createRecord('option', {
            option: optionValue,
            value: option.name
          });
          
          newQuiz.get('options').pushObject(newQuizOption);
        }
      });

      Ember.run.scheduleOnce('afterRender', this, function(){
        $("#"+newListComp.get('id')).draggable({ scroll: false });
      });

      $('#md-quiz').modal('toggle');
      
      this.set('initialised', true);
      
      NProgress.done();
    },

    addQuizOption: function() {
      NProgress.start();

      var count = this.get('QuizOptionCount') + 1;

      this.set('QuizOptionCount', count);

      Ember.$('#OptionsContainer').append(
        '<div class="input-group">' +
        '<span class="input-group-btn">' +
        '<button for="o' +count+ '" type="button" class="btn clickActive"><i class="fa fa-check optionButton"></i></button>' +
        '</span>' +
        '<input id="o' +count+ '" name="0" type="text" class="form-control">' +
        '</div>'
      );

      this.set('initialised', true);

      NProgress.done();
    },

    addContinuation: function(){
      NProgress.start();

      var newContinuation = this.store.createRecord('continuation', {
        toVideo: this.get('ToVideoContinuation')
      });

      var newListComp = this.store.createRecord('listcomp', {
        title: "Continuation",
        comptype: 4,
        starttime: videojs('MainPlayer').getValueSlider().start,
        endtime: videojs('MainPlayer').getValueSlider().end,
        continuation: newContinuation
      });

      this.store.find('video', this.get('ActiveCanvas')).then(function(onVideo) {
        onVideo.get('components').pushObject(newListComp);
      });

      $('#md-continuation').modal('toggle');

      this.set('initialised', true);

      NProgress.done();
    },

    addParallelPlay: function() {
      NProgress.start();

      // Add reference of the Hotspot in the HotspotList
      var parallelHotspot = new fabric.Rect({
        left: 200,
        top: 400,
        fill: 'rgba(255, 30, 81, 0.6)',
        width: 220,
        height: 100,
        scaleX: 2.784954604409857,
        scaleY: 1.3501393548748069
      });

      var newParallel = this.store.createRecord('parallel', {
        parallelVideo: this.get('ParallelPlay'),
        canvasElement: parallelHotspot
      });

      // Draw the hotspot on the ActiveCanvas
      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').add(newParallel.get('canvasElement'));
      });

      var newListComp = this.store.createRecord('listcomp', {
        title: "Parallel Play",
        comptype: 5,
        starttime: this.get('startTimeHandle'),
        endtime: this.get('endTimeHandle'),
        parallel: newParallel
      });

      this.store.find('video', this.get('ActiveCanvas')).then(function(onVideo) {
        onVideo.get('components').pushObject(newListComp);
      });

      $('#md-parallel').modal('toggle');

      this.set('initialised', true);

      NProgress.done();
    },

    addProduct: function () {
      NProgress.start();

      var inputImage = Ember.$("#imageInput")[0].files;

      var objectURL = URL.createObjectURL(inputImage[0]);

      var productElement = new fabric.Rect({
        left: 200,
        top: 400,
        fill: 'rgba(255, 30, 81, 0.6)',
        width: 220,
        height: 100,
        scaleX: 2.784954604409857,
        scaleY: 1.3501393548748069
      });

      var newMultimedia = this.store.createRecord('multimedia', {
        title: this.get('productTitle'),
        description: this.get('productDescription'),
        image: objectURL,
        price: this.get('productPrice'),
        canvasElement: productElement
      });

      var newListComp = this.store.createRecord('listcomp', {
        title: this.get('productTitle'),
        comptype: 6,
        starttime: this.get('startTimeHandle'),
        endtime: this.get('endTimeHandle'),
        multimedia: newMultimedia
      });

      // Draw the hotspot on the ActiveCanvas
      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').add(newMultimedia.get('canvasElement'));
      });

      this.store.find('video', this.get('ActiveCanvas')).then(function(onVideo) {
        onVideo.get('components').pushObject(newListComp);
      });

      $('#md-multimedia').modal('toggle');

      this.set('initialised', true);

      NProgress.done();
    },

    selectMultimedia: function (component) {
      var controller = this;

      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').setActiveObject(component.get('multimedia').get('canvasElement'));
      });

      Ember.$( "#key_"+component.get('multimedia.id') ).keydown(function(key) {
        if (key.keyCode === 68) {
          controller.send('deleteMultimedia', component);
        }
        return false;
      });

      this.store.find('multimedia', component.get('multimedia.id')).then(function(multimedia) {
        controller.set('model.multimedia', multimedia);
        controller.set('model.listcomp', component);
      });
    },

    deleteMultimedia: function (component) {
      NProgress.start();

      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').remove(component.get('multimedia').get('canvasElement'));
      });

      var multimedia = component.get('multimedia.id');
      this.store.find('multimedia', multimedia).then(function(multimedialToDelete) {
        multimedialToDelete.destroyRecord();
      });

      Ember.$("#"+component.get('id')).remove();

      component.destroyRecord();

      NProgress.done();
    },

    selectParallel: function(component) {
      var controller = this;

      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').setActiveObject(component.get('parallel').get('canvasElement'));
      });

      Ember.$( "#key_"+component.get('parallel.id') ).keydown(function(key) {
        if (key.keyCode === 68) {
          controller.send('deleteParallel', component);
        }
        return false;
      });

      this.store.find('parallel', component.get('parallel.id')).then(function(parallel) {
        controller.set('model.parallel', parallel);
        controller.set('model.listcomp', component);
      });
    },

    deleteParallel: function(component) {
      NProgress.start();

      var parallel = component.get('parallel.id');

      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').remove(component.get('parallel').get('canvasElement'));
      });

      this.store.find('parallel', parallel).then(function(parallelToDelete) {
        parallelToDelete.destroyRecord();
      });

      component.destroyRecord();

      NProgress.done();
    },

    selectContinuation: function(component) {
      var controller = this;

      Ember.$( "#key_"+component.get('continuation.id') ).keydown(function(key) {
        if (key.keyCode === 68) {
          controller.send('deleteContinuation', component);
        }
        return false;
      });

      this.store.find('continuation', component.get('continuation.id')).then(function(continuation) {
        controller.set('model.continuation', continuation);
        controller.set('model.listcomp', component);
      });
    },

    deleteContinuation: function(component) {
      NProgress.start();

      var continuation = component.get('continuation.id');

      this.store.find('continuation', continuation).then(function(continuationToDelete) {
        continuationToDelete.destroyRecord();
      });

      component.destroyRecord();

      NProgress.done();
    },

    selectHotspot: function(component) {
      videojs('MainPlayer').currentTime(component.get('starttime'));

      videojs("MainPlayer").setValueSlider(component.get('starttime'), component.get('endtime'));

      var controller = this;

      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').setActiveObject(component.get('hotspot').get('canvasElement'));
      });

      Ember.$( "#key_"+component.get('hotspot.id') ).keydown(function(key) {
        if (key.keyCode === 68) {
          controller.send('deleteHotspot', component);
        }
        return false;
      });

      this.store.find('hotspot', component.get('hotspot.id')).then(function(hotspot) {
        controller.set('model.hotspot', hotspot);
        controller.set('model.listcomp', component);
      });
    },

    deleteHotspot: function (component) {
      NProgress.start();

      var hotspot = component.get('hotspot.id');

      this.store.find('video', this.get('ActiveCanvas')).then(function(video){
        video.get('canvas').remove(component.get('hotspot').get('canvasElement'));
      });

      this.store.find('hotspot', hotspot).then(function(hotspotToDelete) {
        hotspotToDelete.destroyRecord();
      });

      component.destroyRecord();

      NProgress.done();
    },

    selectSocial: function(component) {
      var controller = this;

      videojs('MainPlayer').currentTime(component.get('time'));

      Ember.$( "#key_"+component.get('social.id') ).keydown(function(key) {
        if (key.keyCode === 68) {
          controller.send('deleteSocial', component);
        }
        return false;
      });

      this.store.find('social', component.get('social.id')).then(function(social) {
        controller.set('model.social', social);
        controller.set('model.listcomp', component);
      });
    },

    deleteSocial: function(component) {
      NProgress.start();

      var socialId = component.get('social.id');

      this.store.find('social', socialId).then(function(social) {
        social.destroyRecord();
      });

      Ember.$("#"+component.get('id')).remove();

      component.destroyRecord();

      NProgress.done();
    },

    selectQuiz: function(component) {
      var controller = this;

      videojs('MainPlayer').currentTime(component.get('time'));

      Ember.$( "#key_"+component.get('quiz.id') ).keydown(function(key) {
        if (key.keyCode === 68) {
          controller.send('deleteQuiz', component);
        }
        return false;
      });

      this.store.find('quiz', component.get('quiz.id')).then(function(quiz) {
        controller.set('model.quiz', quiz);
        controller.set('model.listcomp', component);
      });
    },

    deleteQuiz: function(component) {
      NProgress.start();

      this.store.find('quiz', component.get('quiz.id')).then(function(quizToDelete) {
        var options = quizToDelete.get('options');
        var optionsArray = options.toArray();
        optionsArray.forEach(function(option) {
          option.destroyRecord();
          options.removeObject(option);
        });
        quizToDelete.destroyRecord();
      });

      component.destroyRecord();

      Ember.$("#"+component.get('id')).remove();

      NProgress.done();
    },

    editSocial: function() {
      // Update the fields
    },

    editQuiz: function() {
      NProgress.start();

      var controller = this;

      var top = Ember.$('#'+controller.get('model.listcomp.id')).css('top');

      var left = Ember.$('#'+controller.get('model.listcomp.id')).css('left');

      Ember.$("#"+this.get('model.listcomp.id')).remove();

      $('#md-EDITquiz').modal('toggle');

      controller.store.find('quiz', this.get('model.quiz.id')).then(function(quiz) {
        quiz.set('top', top);
        quiz.set('left', left);
        Ember.$("#html_"+controller.get('ActiveCanvas')).append(
          '<section class="quizTemplate panel animated bounceIn" id="' + controller.get('model.listcomp.id') + '" style="top:'+ quiz.get('top') +'; left:'+ quiz.get('left') +'">' +
          '<header class="panel-heading xs">' +
          '<h2><strong>' + quiz.get('question') + '</h2>' +
          '</header>' +
          '<div class="panel-body" id="options_' + controller.get('model.listcomp.id') + '">' +
          '</div>' +
          '</section>'
        );

        quiz.get('options').forEach(function(option) {
          Ember.$('#options_' + controller.get('model.listcomp.id')).prepend(
            '<div class="quizOption">' +
            '<button for="o1" type="button" class="btn clickActive"><i class="fa fa-check optionButton"></i></button>' +
            '<span class="quizOptionText"> ' + option.get('option') + '</span>' +
            '</div>'
          );
        });

        Ember.run.scheduleOnce('afterRender', this, function(){
          $("#"+controller.get('model.listcomp.id')).draggable({ scroll: false });
        });
      });

      NProgress.done();
    },

    UpdateDetails: function () {
      // Update video details.
    },

    // On click change main player source.
    setVideo: function(video) {
      videojs('MainPlayer').src({ type: "video/mp4", src: video.get('file') });

      Ember.$('#MainPlayer').attr('currentPlay', video.get('title'));

      this.set('ActiveCanvas', video.get('id'));

      videojs("MainPlayer").load();

      videojs("MainPlayer").ready(function(){
        videojs("MainPlayer").play();
        Ember.run.later((function() {
          videojs("MainPlayer").setValueSlider(2, videojs("MainPlayer").duration()-2);
        }), 200);
      });

      Ember.$('#canvasContainer').children().hide();

      Ember.$('#htmlContainer').children().hide();

      Ember.$('#'+video.get('id')).closest("div").show();

      Ember.$('#html_'+video.get('id')).show();

      this.set('model.video', video);
    },

    deleteVideo: function (video) {
      NProgress.start();

      video.destroyRecord();

      Ember.$('#'+video.get('id')).closest("div").remove();

      NProgress.done();
    },

    PreviewVideo: function() {
      videojs('MainPlayer').pause();

      this.set('isPreview', true);

      Ember.$('#EditMode').hide();

      Ember.$('#PreviewMode').show();

      Ember.$('#MainCanvas').prepend(Ember.$('<canvas/>',{'id':'PreviewCanvas'}));

      var PreviewCanvas = new fabric.Canvas('PreviewCanvas', { hoverCursor : 'pointer' });

      var controller = this;

      PreviewCanvas.setHeight(908);

      PreviewCanvas.setWidth(1610);

      PreviewCanvas.renderAll();

      this.store.find('video').then(function (videos) {
        var zIndex = 10;
        videos.forEach(function (video) {
          var videoID = "video_" + video.get('id');
          Ember.$("#videoContainer").append(
            '<video id="' + videoID + '" class="video-js vjs-default-skin hidden2" height="auto" width="" style="z-index: '+ zIndex +'" data-setup={}>' +
            '<source src="' + video.get('file') + '" type="video/mp4"></source>' +
            '</video>'
          );
          zIndex--;
          var ticker = new videojs(videoID, {}, function () {});
          video.get('components').forEach(function (component) {
            var checker = true;
            ticker.on('timeupdate', function() {
              if ( ticker.currentTime() >= component.get('starttime') && checker )
              {
                if (component.get('comptype') === 1) {

                  var o1 = component.get('hotspot').get('canvasElement').clone();
                  controller.store.find('hotspot', component.get('hotspot.id')).then(function(hotspot) {

                    var endTime = (component.get('endtime') - component.get('starttime'))*1000;

                    Ember.run.later((function() {
                      PreviewCanvas.remove(o1);
                    }), endTime);

                    o1.on("mousedown", function() {

                      // Clear the canvas layer
                      PreviewCanvas.clear().renderAll();

                      // Play the next video and hide all other players.
                      videojs('video_'+hotspot.get('toVideo.id')).play();
                      videojs(videoID).pause();
                      Ember.$('#videoContainer').children().hide();

                      // Show this player and pause the existing video
                      Ember.$("#video_"+hotspot.get('toVideo.id')).show();

                      Ember.run.later((function() {
                        videojs('video_'+hotspot.get('toVideo.id')).controls(true);
                      }), 100);

                    });
                  });

                  o1.set('hasControls', false);
                  o1.set('hasBorders', false);
                  o1.set('lockMovementX', true);
                  o1.set('lockMovementY', true);

                  PreviewCanvas.add(o1);
                  checker = false;

                } else if ( component.get('comptype') === 2 ) {

                  controller.store.find('quiz', component.get('quiz.id')).then(function(quiz) {

                    var endTime = (component.get('endtime') - component.get('starttime'))*1000;

                    Ember.run.later((function() {
                      Ember.$('#quiz_'+component.get('id')).addClass('animated bounceOut');
                    }), endTime);

                    quiz.set('top',Ember.$('#'+component.get('id')).css('top'));
                    quiz.set('left',Ember.$('#'+component.get('id')).css('left'));
                    Ember.$("#MainCanvas").prepend(
                      '<section class="quizTemplate panel animated bounceIn" id="quiz_' + component.get('id') + '" style="top:'+ quiz.get('top') +'; left:'+ quiz.get('left') +'">' +
                      '<header class="panel-heading xs">' +
                      '<h2><strong>' + quiz.get('question') + '</h2>' +
                      '</header>' +
                      '<div class="panel-body" id="options_' + component.get('id') + '">' +
                      '</div>' +
                      '</section>'
                      );

                    quiz.get('options').forEach(function(option) {
                      if (parseInt(option.get('value')) === 1) {

                        Ember.$('#options_' + component.get('id')).prepend(
                          '<div class="quizOption">' +
                          '<button for="o1" type="button" class="btn clickCorrect"><i class="fa fa-check optionButton"></i></button>' +
                          '<span class="quizOptionText"> ' + option.get('option') + '</span>' +
                          '</div>'
                          );

                      } else {

                        Ember.$('#options_' + component.get('id')).prepend(
                          '<div class="quizOption">' +
                          '<button for="o1" type="button" class="btn clickIncorrect"><i class="fa fa-check optionButton"></i></button>' +
                          '<span class="quizOptionText"> ' + option.get('option') + '</span>' +
                          '</div>'
                          );

                      }
                    });

                  });

                  checker = false;
                } else if ( component.get('comptype') === 3 ) {

                  controller.store.find('social', component.get('social.id')).then(function(social) {

                    var endTime = (component.get('endtime') - component.get('starttime'))*1000;

                    Ember.run.later((function() {
                      Ember.$('#social_'+component.get('id')).addClass('animated bounceOut');
                    }), endTime);


                    social.set('top',Ember.$('#'+component.get('id')).css('top'));
                    Ember.$("#MainCanvas").append(
                      '<div id="social_'+ component.get('id') +'" class="SocialMedia animated bounceIn" style="top:'+ social.get('top') +'; left:'+ social.get('left') +';">' +
                      '<h3>Share this experience with your friends!</h3><Br>' +
                      '<button class="btn btn-primary btn-lg m-r-sm" type="button" onclick="window.open(\'http://'+social.get('facebook')+'\')"><i class="fa fa-facebook"></i> Share it on Facebook</button>' +
                      '<button class="btn btn-theme-inverse btn-lg" type="button" onclick="window.open(\'http://'+social.get('twitter')+'\')"><i class="fa fa-twitter"></i> Share it on Twitter</button>' +
                      '</div>'
                      );
                  });

                  checker = false;

                } else if ( component.get('comptype') === 4 ){
                  controller.store.find('continuation', component.get('continuation.id')).then(function(continuation) {
                    videojs(videoID).on('ended', function() {
                      PreviewCanvas.clear().renderAll();
                      videojs('video_'+continuation.get('toVideo.id')).play();
                      Ember.$('#videoContainer').children().hide();

                      // Show this player and pause the existing video
                      Ember.$("#video_"+continuation.get('toVideo.id')).show();

                      Ember.run.later((function() {
                        videojs('video_'+continuation.get('toVideo.id')).controls(true);
                      }), 500);
                    });
                  });
                } else if ( component.get('comptype') === 5 ){
                  controller.store.find('parallel', component.get('parallel.id')).then(function(parallel) {

                    var endTime = (component.get('endtime') - component.get('starttime'))*1000;

                    Ember.run.later((function() {
                      PreviewCanvas.remove(parallelHotspot);
                    }), endTime);

                    videojs('video_'+parallel.get('parallelVideo.id')).play();

                    var parallelHotspot = component.get('parallel').get('canvasElement').clone();

                    var flip = true;
                    parallelHotspot.on("mousedown", function() {

                      if (flip) {
                        Ember.$('#videoContainer').children().hide();
                        Ember.$("#video_"+parallel.get('parallelVideo.id')).show();
                        flip = false;
                      } else {
                        Ember.$('#videoContainer').children().hide();
                        Ember.$("#"+videoID).show();
                        flip = true;
                      }

                    });

                    parallelHotspot.set('hasControls', false);
                    parallelHotspot.set('hasBorders', false);
                    parallelHotspot.set('lockMovementX', true);
                    parallelHotspot.set('lockMovementY', true);

                    PreviewCanvas.add(parallelHotspot);
                    checker = false;

                  });
                } else if ( component.get('comptype') === 6 ){

                  controller.store.find('multimedia', component.get('multimedia.id')).then(function(multimedia) {

                    var endTime = (component.get('endtime') - component.get('starttime'))*1000;

                    Ember.run.later((function() {
                      PreviewCanvas.remove(o1);
                      Ember.$('#product_'+multimedia.get('id')).addClass('animated bounceOut');
                    }), endTime);

                    multimedia.set('top',Ember.$('#'+component.get('id')).css('top'));
                    multimedia.set('left',Ember.$('#'+component.get('id')).css('left'));

                    var o1 = component.get('multimedia').get('canvasElement').clone();
                    controller.store.find('multimedia', component.get('multimedia.id')).then(function(multimedia) {
                      o1.on("mousedown", function() {

                        Ember.$("#MainCanvas").append(
                          '<section class="producttemplate animated bounceIn" id="product_' + multimedia.get('id') + '" style="top: 50px;">' +
                          '<div class="w-row">' +
                          '<div class="productimage" style="background-image: url(' + multimedia.get('image') + ');"></div>' +
                          '<div class="productdetails">' +
                          '<h3 class="producttitle">' + multimedia.get('title') + '</h3>' +
                          '<div class="productprice">' + multimedia.get('price') + '</div>' +
                          '<p class="productdescription">' + multimedia.get('description') + '</p>' +
                          '<div class="buybutton">' +
                          '<div class="buybuttontext"><i class="fa fa-shopping-cart"></i> Buy Now</div>' +
                          '</div>' +
                          '<i id="closeProduct" class="fa fa-times close-product-info"></i>' +
                          '</div>' +
                          '</div>' +
                          '</section>'
                          );

                      });
                    });

                    o1.set('hasControls', false);
                    o1.set('hasBorders', false);
                    o1.set('lockMovementX', true);
                    o1.set('lockMovementY', true);

                    PreviewCanvas.add(o1);
                    checker = false;

                  });

                }
              }
            });
          });
        });
      });

      Ember.run.scheduleOnce('afterRender', this, function(){
        // Display the top most player
        Ember.$("#videoContainer").children().first().show();

        // show player controls of the top most player
        videojs(Ember.$("#videoContainer").children().first().attr('id')).controls(true);
      });

    },

    HidePreview: function () {

      this.set('isPreview', false);

      Ember.$('#MainCanvas').empty();
      Ember.$('#videoContainer').empty();

      this.store.find('video').then(function (videos) {
        videos.forEach(function (video) {
          videojs('video_'+video.get('id')).dispose();
        });
      });

      Ember.$('#PreviewMode').hide();
      Ember.$('#EditMode').show();
    }
  }
});
