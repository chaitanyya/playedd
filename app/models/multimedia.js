import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  component: DS.belongsTo('listcomp'),
  description: DS.attr(),
  price: DS.attr(),
  image: DS.attr(),
  canvasElement: DS.attr(),
  top: DS.attr(),
  left: DS.attr()
});
