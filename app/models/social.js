import DS from 'ember-data';

export default DS.Model.extend({
  facebook: DS.attr(),
  twitter: DS.attr(),
  top: DS.attr(),
  component: DS.belongsTo('listcomp'),
  left: DS.attr()
});