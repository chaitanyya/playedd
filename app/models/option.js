import DS from 'ember-data';

export default DS.Model.extend({
  question: DS.belongsTo('quiz'),
  option: DS.attr(),
  value: DS.attr(),
  isCorrect: function () {
      if (parseInt(this.get('value')) === 1) {
          return true;
      } else if (parseInt(this.get('value')) === 0) {
          return false;
      }
  }.property('value')
});
