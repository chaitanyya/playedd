import Ember from 'ember';

export default Ember.Route.extend({
	model: function () {
		return Ember.RSVP.hash({
			hotspots: this.store.findAll('hotspot'),
			videos: this.store.findAll('video'),
			quiz: this.store.findAll('quiz'),
			option: this.store.findAll('option'),
			listcomp: this.store.findAll('listcomp'),
			social: this.store.findAll('social'),
			continuation: this.store.findAll('continuation'),
			parallel: this.store.findAll('parallel'),
      multimedia: this.store.findAll('multimedia')
		});
	}
});
