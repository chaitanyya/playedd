import Ember from 'ember';
import EmberUploader from 'ember-uploader';

export default EmberUploader.FileField.extend({
  multiple: true,
  store: Ember.inject.service(),
  filesDidChange: function(files) {
    if (!Ember.isEmpty(files)) {
      NProgress.start();

      // Run a for loop on the list of files.
      for (var i = files.length - 1; i >= 0; i--) {
        // Conver the file object into Blob URL
        var objectURL = URL.createObjectURL(files[i]);

        // Get the name of the files uploaded
        var fileName = files[i].name;
        var newName = fileName.substr(0, fileName.indexOf('.'));

        // Create a new Ember Data object (Local Storage)
        var newVideo = this.get('store').createRecord('video', {
          title: newName,
          file: objectURL
        });

        Ember.$('#htmlContainer').prepend('<div id="html_'+ newVideo.get('id') +'"></div>');

        // Add a canvas element
        Ember.$('#canvasContainer').prepend( Ember.$('<canvas/>',{'id':newVideo.get('id')}) );
        newVideo.set('canvas', new fabric.Canvas(newVideo.get('id')));

        // Modify Fabric.js presets
        fabric.Object.prototype.set({
          transparentCorners: false,
          borderColor: '#038EEA',
          cornerColor: '#038EEA',
        });

        // Change Canvas Size
        newVideo.get('canvas').setHeight(908);
        newVideo.get('canvas').setWidth(1610);
        newVideo.get('canvas').renderAll();
      }
      NProgress.done();
    }
  }
});
