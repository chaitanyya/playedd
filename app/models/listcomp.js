import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  onVideo: DS.belongsTo('video'),
  starttime: DS.attr(),
  endtime: DS.attr(),
  comptype: DS.attr(),
  quiz: DS.belongsTo('quiz'),
  hotspot: DS.belongsTo('hotspot'),
  social: DS.belongsTo('social'),
  continuation: DS.belongsTo('continuation'),
  parallel: DS.belongsTo('parallel'),
  multimedia: DS.belongsTo('multimedia'),
  isHotspot: function () {
      if (this.get('comptype') === 1) {
          return true;
      } else {
          return false;
      }
  }.property('comptype'),
  isQuiz: function () {
      if (this.get('comptype') === 2) {
          return true;
      } else {
          return false;
      }
  }.property('comptype'),
  isSocial: function () {
      if (this.get('comptype') === 3) {
          return true;
      } else {
          return false;
      }
  }.property('comptype'),
  isContinuation: function () {
      if (this.get('comptype') === 4) {
          return true;
      } else {
          return false;
      }
  }.property('comptype'),
  isParallel: function () {
      if (this.get('comptype') === 5) {
          return true;
      } else {
          return false;
      }
  }.property('comptype'),
  isMultimedia: function () {
      if (this.get('comptype') === 6) {
          return true;
      } else {
          return false;
      }
  }.property('comptype')

});
