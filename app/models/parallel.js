import DS from 'ember-data';

export default DS.Model.extend({
  parallelVideo: DS.belongsTo('video', {inverse: null}),
  component: DS.belongsTo('listcomp'),
  canvasElement: DS.attr()
});
