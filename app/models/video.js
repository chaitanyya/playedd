import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  file: DS.attr(),
  canvas: DS.attr(),
  components: DS.hasMany('listcomp')
});
