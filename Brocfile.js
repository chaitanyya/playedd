/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp();

// Use `app.import` to add additional libraries to the generated
// output files.

// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.

// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

// StyleSheets
app.import('vendor/StyleSheet/bootstrap/bootstrap.min.css');
app.import('vendor/StyleSheet/bootstrap/bootstrap-themes.css');
app.import('vendor/StyleSheet/font-awesome/font-awesome.min.css');
app.import('vendor/JavaScript/datetime/datetime.css');
app.import('vendor/StyleSheet/animation.css');
app.import('vendor/JavaScript/dropupload/css/basic.css');
app.import('vendor/JavaScript/dropupload/css/dropzone.css');
app.import('vendor/JavaScript/tour/css/bootstrap-tour.css');
app.import('vendor/JavaScript/jquery-ui/jquery-ui.css');

// Custom StyleSheets
app.import('vendor/StyleSheet/style.css');
app.import('vendor/StyleSheet/styleTheme4.css');

// JavaScripts
app.import('vendor/JavaScript/jquery.min.js');
app.import('vendor/JavaScript/bootstrap/bootstrap.min.js');
app.import('vendor/JavaScript/modernizr/modernizr.js');
app.import('vendor/JavaScript/mmenu/jquery.mmenu.js');
app.import('vendor/JavaScript/form/form.js');
app.import('vendor/JavaScript/datetime/datetime.js');
app.import('vendor/JavaScript/chart/chart.js');
app.import('vendor/JavaScript/pluginsForBS/pluginsForBS.js');
app.import('vendor/JavaScript/dropupload/dropzone-amd-module.js');
app.import('vendor/JavaScript/tour/js/bootstrap-tour.js');
app.import('vendor/JavaScript/jquery-ui/jquery-ui.js');

app.import('bower_components/fabric/dist/fabric.js');
app.import('bower_components/video.js/dist/video-js/video.js');
app.import('bower_components/video.js/dist/video-js/video-js.css');
app.import('bower_components/animate.css/animate.css');
app.import('bower_components/nprogress/nprogress.css');
app.import('bower_components/nprogress/nprogress.js');

app.import('vendor/JavaScript/rangeselector/rangeslider.js');
app.import('vendor/JavaScript/rangeselector/rangeslider.css');

// Custom playedd JavaScript
app.import('vendor/JavaScript/playedd.custom.js');

module.exports = app.toTree();
